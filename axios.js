/*
 * 支持的功能
 *   1.支持全局默认配置项 _ajax.defaults.xxx=xxx
 *   2.发送请求_ajax.get/post...
 *   3.每一次请求都会返回PROMISE实例，基于PROMISE设计模式进行管理
 *   4.支持_ajax.all
 */
(function () {
    //发送ajax请求，且基于promise进行管理
    class Axios{
        constructor(url,options = {}){
            this.url = url
            this.options = options
            return this.initAjax()
        }
        //发送ajax请求
        initAjax () {
            let {
				url,
                options: {baseUrl,withCredentials,headers,transformRequest,transformResponse,validateStatus,
                    params,data,cache,method
                }
            } = this
            //保证响应拦截器中信息的合法性
            Array.isArray(transformResponse) ? null : transformResponse = []
            new Array(2).fill(null).forEach((item,index)=> {
                //判断transformResponse[index]是不是函数，是函数则什么也不做，不是把该项赋为null
                typeof transformResponse[index] === 'function' ? null : transformResponse[index] = null
            })
            return new Promise((resolve,reject)=>{
                let xhr = new XMLHttpRequest()
                //url处理
                url = baseUrl + url//拼接完整url
                if(/^(GET|DELETE|HEAD|OPTIONS)$/i.test(method)) {
                    if(params){//是get系列请求且配置了请求参数
                        let result = ''
                        for (const key in params) {
                            if (params.hasOwnProperty(key)) {
                                result += `&${key}=${params[key]}`
                            }
                            result = result.substring(1)
                        }
                        url += `${url.indexOf('?') === -1 ? '?' : '&'}${result}`                       
                        if(cache) {//不缓存get请求
                            url += `_=${Math.random()}`
                        }
                    }
                }
                xhr.open(method,url)
                xhr.onreadystatechange = function () {
                    let flag = validateStatus(xhr.status)
                    if(flag){//成功请求到数据
                        if(xhr.readyState === 4){
                            let resHeader = {}
                            xhr.getAllResponseHeaders().split(/\n/).forEach(item=>{
                                let [key='',value=''] = item.split(':')
                                if(key.trim() === '') return
                                resHeader[key.trim()] = value.trim()
                            })
                            resolve({
                                status: xhr.status,
							    statusText: xhr.statusText,
							    request: xhr,
							    data: JSON.parse(xhr.responseText),
							    headers: resHeader
                            })
                        }
                    }else{//未成功请求到数据
                        reject({
                            status: xhr.status,
                            statusText: xhr.statusText,
                            request: xhr
                        })
                        return
                    }
                }
                //跨域处理
				xhr.withCredentials = withCredentials;
                //设置请求头
                if(headers){
                    for (const key in headers) {
                        if (headers.hasOwnProperty(key)) {
                            xhr.setRequestHeader(key, encodeURI(headers[key]))
                        }
                    }
                }
                //请求拦截器：请求主体传递信息的拦截
                if((/^(POST|PUT)$/i.test(method))){
                    typeof transformRequest === 'function' ? data = transformRequest(data) : data = null
                }
                xhr.send(data)
            }).then(...transformResponse)
        }
    }
    //创建ajax管理调用
    function _axios () {}
    //参数初始化
    function _init (options = {}) {
        //headers需要特殊处理（把用户OPTIONS中传递的HEADERS，和DEFAULTS中的HEADERS进行合并，而不是整体替换），
        //其余的配置项直接用OPTIONS中的替换
        options.headers ? _axios.defaults.headers = options.headers : _axios.defaults.headers
        delete options.headers
        return Object.assign(_axios.defaults,options)
    }
    //创建_axios的全局默认配置项
    _axios.defaults = {
        baseUrl: '',//基础路径
        withCredentials: false,//是否携带传输凭证（cookie）
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },//请求头
        transformRequest: function (data) {
            if(!data) return
            let str = ''
            for (const key in data) {
                if (data.hasOwnProperty(key)) {
                    str += `&${key}=${data[key]}`
                }
            }
            str = str.substring(1)
            return str
        },//请求体配置
        transformResponse: [function onFulfilled(response) {
            return response.data//返回请求到的数据
        },function onReject(reject) {
            return Promise.reject(reject)//返回错误信息
        }],//响应体配置
        validateStatus: function (status) {
            return /^(2|3)\d{2}$/.test(status)
        },//响应状态码是200-400之间都算成功
        //=>请求配置项
		params: {},//get请求在url地址中通过'?'传参的方式传递给服务器的数据
		data: {},//post请求在请求主体中传递给服务器的数据
		cache: true//get请求是否缓存，默认不缓存
    }
    //get系列请求
    let getArr = ['get','head','options','delete']
    getArr.forEach((item)=>{
        _axios[item] = function (url,options = {}) {
            options.method = item
            return new Axios(url,_init(options))//用户通过_axios调用get方法后，应返回一个promise实例
        }        
    })
    //post系列请求
    let postArr = ['post','put']
    postArr.forEach((item)=>{
        _axios[item] = function(url,data = {},options = {}){
            options.method = item
            options.data = data
            return new Axios(url,_init(options))
        }
    })
    //_axios的all方法
    _axios.all = function (promiseArr = []) {
        return Promise.all(promiseArr)//直接调用promise类的all方法
    }
    window._axios = _axios
})()
//_axios.all([Promise.resolve(100),Promise.resolve(200)]).then(result => console.log(result))